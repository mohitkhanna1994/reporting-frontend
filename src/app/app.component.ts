import { Component,OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from './service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'reporting-front';
  isLoggedIn$: Observable<boolean>;  
  
  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isUserLoggedIn;
  }
}
