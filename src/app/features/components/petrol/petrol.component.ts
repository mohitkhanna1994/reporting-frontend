import { Component, OnInit } from '@angular/core';
import { PetrolService } from 'src/app/service/petrol/petrol.service';
import { UserService } from 'src/app/service/user/user.service';

@Component({
  selector: 'app-petrol',
  templateUrl: './petrol.component.html',
  styleUrls: ['./petrol.component.css']
})
export class PetrolComponent implements OnInit {

  public selectedUser;
  public selectedDate;
  public users: any[];
  public endkm;
  public startkm;

  constructor(private petrolService: PetrolService, private userService: UserService) { }

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers() {
    this.userService.getUsers().subscribe((result) => {
      this.users = result;
      console.log(this.users);
    },(err)=>{
      console.log(err)
    })
  }

  handleSumbit() {

    if (this.selectedUser && this.startkm && this.endkm && this.selectedDate) {
      let request = {
        startReading: this.startkm,
        endReading: this.endkm,
        date: this.selectedDate.toISOString().substring(0,10),
        userName: this.selectedUser
      }
      this.petrolService.addPetrol(request).subscribe((res) => console.log(res),err => console.log(err));
    }
  }
}
