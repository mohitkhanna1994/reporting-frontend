import { Component, OnInit } from '@angular/core';
import { AttendanceService } from 'src/app/service/attendance/attendance.service';
import { UserService } from 'src/app/service/user/user.service';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {


  public selectedUser;
  public selectedDateTime;
  public users: any[];
  public selectedOption;

  constructor(private attendanceService: AttendanceService, private userService: UserService) { }

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers() {
    this.userService.getUsers().subscribe((result) => {
      this.users = result;
      console.log(this.users);
    }, err => {
      console.log(err)
    })
  }

  handleSumbit() {
    if (this.selectedDateTime && this.selectedOption && this.selectedUser) {
      let sIn = false;
      let sOut = false;
      if (this.selectedOption == 'signIn') {
        sIn = true;
      } else {
        sOut = true;
      }
      let request = {
        signIn: sIn,
        signOut: sOut,
        signInTime: this.selectedDateTime,
        signOutTime: this.selectedDateTime,
        userName: this.selectedUser
      }
      console.log(request)
      this.attendanceService.addAttendanceByAdmin(request).subscribe((res) => {
        console.log(res)
      }, (err) => {
        console.log(err)
      })
    }
  }
}
