import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AboutComponent } from "./components/about/about.component";
import { AttendanceComponent } from "./components/attendance/attendance.component";
import { ContactUsComponent } from "./components/contact-us/contact-us.component";
import { PetrolComponent } from "./components/petrol/petrol.component";


const routes: Routes = [
    {path:'', redirectTo:'petrol',pathMatch:'full'},
    {path:'petrol',component:PetrolComponent},
    {path:'attendance',component:AttendanceComponent},
    {path:'contact',component:ContactUsComponent},
    {path:'about',component:AboutComponent}
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class FeatureRoutingModule { }