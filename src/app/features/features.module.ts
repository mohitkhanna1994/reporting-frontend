import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PetrolComponent } from './components/petrol/petrol.component';
import { AttendanceComponent } from './components/attendance/attendance.component';
import { FeatureRoutingModule } from './features-routing.module';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { AboutComponent } from './components/about/about.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  declarations: [PetrolComponent, AttendanceComponent, ContactUsComponent, AboutComponent],
  imports: [
    CommonModule,
    FormsModule,
    FeatureRoutingModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatDatepickerModule,
    MatRadioModule,
    MatNativeDateModule

  ]
})
export class FeaturesModule { }
