export class Constants {

    public static readonly API_URL = {
        BASE_URL: 'http://ec2-3-109-214-33.ap-south-1.compute.amazonaws.com:9090',
        LOGIN_URL: '/api/public/v1/login',
        GET_USERS_URL: '/api/secure/v1/get-user',
        ADD_PETROL_ADMIN_URL: '/api/secure/v1/add-fuel-admin',
        ADD_ATTENDANCE_BY_ADMIN_URL: '/api/secure/v1/add-attendance-admin'
    }

    public static readonly LOCAL_STORAGE = {
        USER_NAME_SESSION_ATTRIBUTE_NAME : 'authenticatedUser',
        USER_TOKEN : 'accessToken',
    }
}