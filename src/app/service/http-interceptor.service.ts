import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service'
import { Constants } from '../util/constants';
import { catchError, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService {
    constructor(private authService: AuthenticationService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        let fullReqUrl = Constants.API_URL.BASE_URL + req.url;
        if (req.url.includes('secure')) {
            const authReq = req.clone({
                headers: new HttpHeaders({
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem(Constants.LOCAL_STORAGE.USER_TOKEN)}`
                }),
                url: fullReqUrl
            });
            return next.handle(authReq).pipe(
                map(res => res),
                catchError(err => this.handleError(err))
            )
        } else {
            const publicReq = req.clone({ url: fullReqUrl });
            return next.handle(publicReq).pipe(
                map(res => res),
                catchError(err => this.handleError(err))
            )
        }
    }

    handleError(err: HttpErrorResponse): any {
        if(!this.authService.userLoggedIn()){
            this.authService.logout();
        }else {
            switch(err.status) {
                case 401: 
                    this.authService.logout();
                    break;
            }
        }
        return throwError(err);
    }
}
