import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from 'src/app/util/constants';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  constructor(private http: HttpClient) { }

  addAttendanceByAdmin(request) {
    return this.http.post<any>(Constants.API_URL.ADD_ATTENDANCE_BY_ADMIN_URL, request);
  }
}
