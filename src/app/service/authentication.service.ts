import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Constants } from '../util/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  
  public username: String;
  public password: String;
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private httpClient:HttpClient,private router: Router) { }

  authenticationService(username: String, password: String) {
   return  this.httpClient.post<any>(Constants.API_URL.LOGIN_URL,{"userName":username,"password":password});
  }

  createBasicAuthToken(userName:String,password: String) {
    return 'Basic '+window.btoa(userName+ ":"+password);
  }

  registerSuccessfulLogin(token,userName ){
    localStorage.setItem(Constants.LOCAL_STORAGE.USER_TOKEN, token);
    localStorage.setItem(Constants.LOCAL_STORAGE.USER_NAME_SESSION_ATTRIBUTE_NAME, userName);
  }

  logout() {
    localStorage.removeItem(Constants.LOCAL_STORAGE.USER_NAME_SESSION_ATTRIBUTE_NAME);
    localStorage.removeItem(Constants.LOCAL_STORAGE.USER_TOKEN);
    this.username = null;
    this.password = null;
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }

  get isUserLoggedIn() {
    let user = localStorage.getItem(Constants.LOCAL_STORAGE.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null){
      this.loggedIn.next(false);
    }else {
      this.loggedIn.next(true);
    }
    return this.loggedIn.asObservable();
  }

  getLoggedInUserName() {
    let user = localStorage.getItem(Constants.LOCAL_STORAGE.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return ''
    return user
  }

  userLoggedIn() {
    let user = localStorage.getItem(Constants.LOCAL_STORAGE.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return false;
    return true;
  }
}
