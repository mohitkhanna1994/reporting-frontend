import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from '../../util/constants';

@Injectable({
  providedIn: 'root'
})
export class PetrolService {

  constructor(private http: HttpClient) { }

  public addPetrol(request) {
    return this.http.post<any>(Constants.API_URL.ADD_PETROL_ADMIN_URL,request);
  }
}
