import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit {

  isLoggedIn: boolean;  

  constructor(private router: Router,private authService: AuthenticationService) { }

  ngOnInit():void {
    this.isLoggedIn = true;
  }

  onLogout() {
    this.authService.logout(); 
  }
}
