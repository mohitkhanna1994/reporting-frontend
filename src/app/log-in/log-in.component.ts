import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../service/authentication.service';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  hide = true;
  user: string;
  pass : string;
  errorMessage = 'Invalid Credentials';
  successMessage: string;
  invalidLogin = false;
  loginSuccess = false;

  constructor(private router: Router, private authService: AuthenticationService,
    private route: ActivatedRoute,) { 
    }

  ngOnInit(): void {
  }

  handleLogin() {
    console.log(this.user,this.pass);
    this.authService.authenticationService(this.user,this.pass).subscribe((result) =>{
      this.invalidLogin = false;
      this.loginSuccess = true;
      this.successMessage = 'Login Successful.';
      this.authService.registerSuccessfulLogin(result.data.token,result.data.userName)
      this.router.navigate(['/home/features/petrol']);
    }, () => {
      this.invalidLogin = true;
      this.loginSuccess = false;
    });      
  }

}
